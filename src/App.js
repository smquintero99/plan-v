import React, { Component } from 'react';import { HashRouter, Route} from 'react-router-dom';
import AppHeader from './components/AppHeader';import Home from './views/Home';
import Module1 from './views/Module1';import Module2 from './views/Module2';
import Module3 from './views/Module3';import Module4 from './views/Module4';
import Module5 from './views/Module5';import Module6 from './views/Module6';
import Activity1 from './views/Activity1';import Activity2 from './views/Activity2';
import Activity3 from './views/Activity3';import Activity4 from './views/Activity4';
import Activity5 from './views/Activity5';import Activity6 from './views/Activity6';
import Activity7 from './views/Activity7';import Activity8 from './views/Activity8';
import Activity9 from './views/Activity9';import Activity10 from './views/Activity10';
import Activity11 from './views/Activity11';import Activity12 from './views/Activity12';
import Activity14 from './views/Activity14';

class App extends Component {render() {return (<HashRouter><div className="App"><AppHeader />
 <Route exact path='/' component={Home}/><Route exact path='/module/1' component={Module1}/>
 <Route exact path='/module/2' component={Module2}/><Route exact path='/module/3' component={Module3}/>
 <Route exact path='/module/4' component={Module4}/><Route exact path='/module/5' component={Module5}/>
 <Route exact path='/module/6' component={Module6}/><Route exact path='/activity/1' component={Activity1}/>
 <Route exact path='/activity/2' component={Activity2}/><Route exact path='/activity/3' component={Activity3}/>
 <Route exact path='/activity/4' component={Activity4}/><Route exact path='/activity/5' component={Activity5}/>
 <Route exact path='/activity/6' component={Activity6}/><Route exact path='/activity/7' component={Activity7}/>
 <Route exact path='/activity/8' component={Activity8}/><Route exact path='/activity/9' component={Activity9}/>
 <Route exact path='/activity/10' component={Activity10}/><Route exact path='/activity/11' component={Activity11}/><Route exact path='/activity/12' component={Activity12}/>
 <Route exact path='/activity/14' component={Activity14}/> </div></HashRouter>);}}export default App;
