import React,{Component} from 'react'; import 'bulma-switch/dist/css/bulma-switch.min.css'
import VirtueModal from '../components/VirtueModal'; class TableActivity12 extends Component{
 state=this.createStateForSwitches()
 createStateForSwitches(){let dict={}; this.props.activities.forEach(activity=>{dict[activity+1]=false; dict[activity+2]=false; dict[activity+3]=false}); return dict}
 setLegend(activity){let condition1 = this.state[activity.toString()+1]; let condition2 = this.state[activity.toString()+2] 
  let condition3 = this.state[activity.toString()+3]; if(condition1 == true && condition2 == true && condition3 == true){return 'Great attraction, thanks and admiration towards your partner.'}else if(condition1 == true && condition2 == true && condition3 == false){return 'Approach and admiration to your partner. Hurry to take this virtue to level sufficiency.'}else if(condition1 == true && condition2 == false && condition3 == false){return 'High risk of distancing from your partner. Disillusion and weakening of attraction.'}else if(condition1 == true && condition2 == false && condition3 == true){return 'Great attraction and admiration towards your partner.'}else if(condition1 == false && condition3 == true){return 'This virtue in sufficiency allows the establishment of an integral relationship.'}else if(condition1 == false && condition3 == false){return 'Develop this virtue quickly through daily life project exercises.'}}
 switchState = e =>{let newState = !this.state[e.target.id]; this.setState({[e.target.id]:newState}); console.log(this.state)}
 displaySwitch2(activity){if(this.state[activity.toString()+1]==false){return true}else{return false}}
 tablerows(){console.log(this.state); return this.props.activities.map(activity=>{ return <tr><td>{activity}</td><td>
<div className="field" style={{textAlign:'center'}}><input id={activity + '1'} type="checkbox" name="switchExample" className="switch" checked={this.state[activity.toString()+1]}/>
  <label id={activity+1} htmlFor={activity + '1'} onClick={this.switchState}></label></div></td><td>
<div className="field" style={{textAlign:'center'}} hidden={this.displaySwitch2(activity)}><input id={activity + '2'} type="checkbox" name="switchExample" className="switch" checked={this.state[activity.toString()+2]}/>
  <label id={activity+2} htmlFor={activity + '2'}  onClick={this.switchState}></label></div></td><td>
<div className="field" style={{textAlign:'center'}}><input id={activity + '3'} type="checkbox" name="switchExample" className="switch" checked={this.state[activity.toString()+3]}/>
  <label id={activity+3} htmlFor={activity + '3'} onClick={this.switchState}></label></div></td><td>{this.setLegend(activity)}
</td></tr>})}
 render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
  <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
    <th style={{backgroundColor:'#DDD'}}>
     <p className="title is-6" style={{color:'#335e92'}}>My virtues to develop:</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Does my partner have this virtue developed to excellence?</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Have I developed this virtue thanks to the presence of my partner?</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Thanks to my partner or other life experiences, have I developed this virtue at a sufficient level?</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Legend</p></th></tr></thead>
   <tbody>{this.tablerows()}</tbody></table></div></div>)}}
export default TableActivity12;

