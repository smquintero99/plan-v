import React,{Component} from 'react'; class SimpleInfoCard extends Component{render(){return(
<div className="columns"  style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}><div className={this.props.cardClass}>
 <div className="card" style={{backgroundColor:'#335e92'}}><div className="card-content">
  <p className="subtitle is-6 has-text-white">{this.props.text}</p></div></div></div></div>)}};export default SimpleInfoCard;
