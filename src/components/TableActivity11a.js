import React,{Component} from 'react'; import 'bulma-checkradio/dist/css/bulma-checkradio.min.css'
import VirtueModal from '../components/VirtueModal'; class TableActivity11a extends Component{
 tablerows(){return this.props.activities.map(activity=>{ return <span class="tag is-primary is-medium" style={{marginLeft:'10px',marginTop:'5px',marginBottom:'5px'}}>{activity}</span>})}
 tablerows1(){return this.props.activities1.map(activity=>{ return <span class="tag is-primary is-medium" style={{marginLeft:'10px',marginTop:'5px',marginBottom:'5px'}}>{activity}</span>})}
 render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
  <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
    <th style={{backgroundColor:'#DDD'}}>
     <p className="title is-6" style={{color:'#335e92'}}>Number of impacted beings</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Docenas</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Hundreds</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Thousands</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Millions</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Infinite</p></th></tr></thead>
   <tbody><tr><td>Well-being of strangers</td>
     <td><input class="is-checkradio" type="radio" name="answer" id="exampleRadioInline1" />
      <label for="exampleRadioInline1" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer" id="exampleRadioInline2" />
      <label for="exampleRadioInline2" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer" id="exampleRadioInline3" />
      <label for="exampleRadioInline3" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer" id="exampleRadioInline4" />
      <label for="exampleRadioInline4" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer" id="exampleRadioInline5" />
      <label for="exampleRadioInline5" style={{marginLeft:'40%'}}></label></td></tr>
    <tr><td>Amount of personal gifts</td>
     <td><input class="is-checkradio" type="radio" name="answer1" id="exampleRadioInline11" />
      <label for="exampleRadioInline11" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer1" id="exampleRadioInline21" />
      <label for="exampleRadioInline21" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer1" id="exampleRadioInline31" />
      <label for="exampleRadioInline31" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer1" id="exampleRadioInline41" />
      <label for="exampleRadioInline41" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer1" id="exampleRadioInline51" />
      <label for="exampleRadioInline51" style={{marginLeft:'40%'}}></label></td></tr>
    <tr><td>Quantity of essential virtues</td>
     <td><input class="is-checkradio" type="radio" name="answer2" id="exampleRadioInline12" />
      <label for="exampleRadioInline12" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer2" id="exampleRadioInline22" />
      <label for="exampleRadioInline22" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer2" id="exampleRadioInline32" />
      <label for="exampleRadioInline32" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer2" id="exampleRadioInline42" />
      <label for="exampleRadioInline42" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer2" id="exampleRadioInline52" />
      <label for="exampleRadioInline52" style={{marginLeft:'40%'}}></label></td></tr>
    <tr><td>Polarity of essential virtues</td>
     <td><input class="is-checkradio" type="radio" name="answer3" id="exampleRadioInline13" />
      <label for="exampleRadioInline13" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer3" id="exampleRadioInline23" />
      <label for="exampleRadioInline23" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer3" id="exampleRadioInline33" />
      <label for="exampleRadioInline33" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer3" id="exampleRadioInline43" />
      <label for="exampleRadioInline43" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer3" id="exampleRadioInline53" />
      <label for="exampleRadioInline53" style={{marginLeft:'40%'}}></label></td></tr>
    <tr><td>Size of creations</td>
     <td><input class="is-checkradio" type="radio" name="answer4" id="exampleRadioInline14" />
      <label for="exampleRadioInline14" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer4" id="exampleRadioInline24" />
      <label for="exampleRadioInline24" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer4" id="exampleRadioInline34" />
      <label for="exampleRadioInline34" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer4" id="exampleRadioInline44" />
      <label for="exampleRadioInline44" style={{marginLeft:'40%'}}></label></td>
     <td><input class="is-checkradio" type="radio" name="answer4" id="exampleRadioInline54" />
      <label for="exampleRadioInline54" style={{marginLeft:'40%'}}></label></td></tr>
   </tbody></table></div></div>)}}
export default TableActivity11a;
