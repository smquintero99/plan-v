import React,{Component} from 'react'; class TableActivity10 extends Component{
getResult(){let arr=[1,2,3,4]; const sumValue = arr.map(a=>document.getElementById(this.props.id+a).value===''?0:
   parseFloat(document.getElementById(this.props.id+a).value)).reduce((a,b) => a + b, 0); 
  const lenValue = arr.map(a=>document.getElementById(this.props.id+a).value===''?0:1).reduce((a,b) => a + b, 0);
  if(lenValue===4){document.getElementById(this.props.id+0).innerHTML=Math.round(sumValue/lenValue*10)/10}}
tablerows=()=>{let counter = 0; return this.props.activities.map(activity=>{counter++; return <tr><td>
 <p className="subtitle is-6" style={{marginTop:'10px'}}>{activity}</p></td>
 <td style={{width:'10%'}}><input className="input" type="number" id={this.props.id+counter}
  onChange={(e)=>{this.getResult()}} max="3" min="1" step="0.2"/></td></tr>})}
render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
 <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
  <th style={{backgroundColor:'#DDD'}} colSpan={2}>
   <p className="title is-5" style={{color:'#335e92'}}>{this.props.header}</p></th></tr></thead>
  <tbody>{this.tablerows()}<tr><td style={{backgroundColor:'#DDD'}}>
    <p className="title is-6" style={{color:'#870404'}}>{this.props.footer}</p></td>
   <td id={this.props.id+0} style={{textAlign:'center'}}></td></tr></tbody>
</table></div></div>)}}export default TableActivity10;

