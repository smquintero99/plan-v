import React,{Component} from 'react'; import VirtueModal from '../components/VirtueModal'; 
class TableActivity5 extends Component{state={hasResult:false, virtuesToDevelop:[], selectedVirtues:[]}
launchVirtueModal(id){document.getElementById('virtueModal'+id).classList.add('is-active')};
selectedVirtues = (virtues) => {return this.setState({selectedVirtues:virtues})};
tableSubmit=()=>{let virtues = this.state.selectedVirtues; let virtuesToDevelop = virtues.map(virtue=> virtue); 
 this.setState({hasResult:true, virtuesToDevelop:virtuesToDevelop}); return this.props.setResult(virtuesToDevelop)};
render(){const tableResult = <tr><td colSpan="4">{this.state.virtuesToDevelop.map(virtue=> <p>{virtue}</p>)}</td></tr>
const tableButtons =  <tr><td colSpan="4">
 <a className="button is-medium is-pulled-right" style={{backgroundColor:"#870404", color:"#FFF", fontSize:'1rem'}} onClick={this.tableSubmit}>Save</a>
 <a className="button is-medium is-pulled-right" style={{marginLeft:'10px',marginRight:'10px', fontSize:'1rem'}}>Cancel</a></td></tr>
 const selectedVirtues = <div>{this.state.selectedVirtues.map(virtue=>
  <span className="tag is-primary is-medium" style={{marginLeft:'10px',marginTop:'5px',marginBottom:'5px'}}>{virtue}</span>)}</div>
return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}><div className="column is-12">
 <table className="table is-bordered" style={{width:'100%'}}><thead><tr>
  <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">I have the expectation that I force myself to:</p></th>
  <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">To achieve approval of / to show you to:</p></th>
  <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">Making him think that I own / am:</p></th>
  <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">I use the expectation to motivate me:</p></th></tr></thead>
  <tbody><tr><td><input className="input" type="text"/></td>
   <td><div className="select" style={{width:'100%'}}><select style={{width:'100%'}}><option selected hidden>Choose</option>
    <option>Couple</option><option>Son</option><option>Father</option><option>Mother</option><option>Brother</option>
    <option>Grandfather</option><option>Uncle</option><option>Cousin</option><option>Friend</option>
    <option>Boss</option><option>Collaborator</option><option>Partner</option><option>Ex partner</option>
    <option>Employee</option><option>People</option><option>Customers</option><option>Myself</option></select></div></td>
   <td style={{paddingTop:'15px'}} className="has-text-centered">{selectedVirtues}<a onClick={()=>this.launchVirtueModal(this.props.id)}>Add a virtue</a></td>     
   <td><div className="select" style={{width:'100%'}}><select style={{width:'100%'}}><option selected hidden>Choose</option><option>Yes</option><option>Sometimes</option>
    <option>No</option><option>Not Anymore</option></select></div></td></tr>
    {this.state.hasResult?tableResult:tableButtons}</tbody></table></div>
    <VirtueModal id={this.props.id} selectedVirtues={this.selectedVirtues} modalClass="modal"/></div>)}}export default TableActivity5;
