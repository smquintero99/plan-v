import React,{Component} from 'react'; import EmotionModal from '../components/EmotionModal'; class TableActivity9 extends Component{
launchEmotionModal(){document.getElementById('emotionModal').classList.add('is-active')}
render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}><div className="column is-12">
 <table className="table is-bordered" style={{width:'100%'}}><thead><tr>
  <th style={{backgroundColor:'#DDD'}} colSpan={6}><p className="title is-5" style={{color:'#335e92'}}>{this.props.virtue}<span className="subtitle is-6" style={{fontWeight:300}}>&nbsp;&nbsp;&nbsp;{this.props.subtitle}</span></p></th></tr></thead>
  <tbody><tr><td>Everyday / Today</td><td>Every Week / This week</td><td>Every Month / This Month</td><td>Every Three Months / This Trimester</td><td>Each Year / This Year</td><td>At least once in a lifetime</td></tr><tr><td><input className="input" type="text"/></td><td><input className="input" type="text"/></td><td><input className="input" type="text"/></td><td><input className="input" type="text"/></td><td><input className="input" type="text"/></td><td><input className="input" type="text"/></td></tr></tbody></table></div></div>)}}export default TableActivity9;

