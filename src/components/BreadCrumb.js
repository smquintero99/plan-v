import React,{Component} from 'react'; import {Link} from 'react-router-dom';class BreadCrumb extends Component{render(){return(
<div className="columns has-background-white;" style={{marginLeft:'5%', marginRight:'5%', marginTop:'1%', marginBottom:'3%'}}>
 <div className="column is-7"><nav className="breadcrumb" aria-label="breadcrumbs"><ul><li><Link to="/">Home</Link></li>
  <li className="is-active"><a href="#">{this.props.active}</a></li></ul></nav></div></div>
)}};export default BreadCrumb;
