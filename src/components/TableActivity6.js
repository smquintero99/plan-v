import React,{Component} from 'react'; import EmotionModal from '../components/EmotionModal'; 
class TableActivity6 extends Component{
 state={selectedEmotions:[], virtuesToDevelop:[], hasResult:false,
  emotionsToVirtuesMap:{Disappointed:'Acceptance',Deceived:'Acceptance',Frustrated:'Acceptance',
   Envious:'Admiration',Sad:'Joyfulness',Melancholic:'Joyfulness',Crowded:'Companionship',
   Invaded:'Companionship','Grossed out':'Compassion',Guilty:'Dignity',Hurt:'Dignity',Ignored:'Dignity',
   Incompetent:'Dignity',Insufficient:'Dignity',Judged:'Dignity',Despised:'Dignity',Lazy:'Discipline',
   Procrastinating:'Discipline',Scarcity:'Generosity',Robed:'Generosity',Disoriented:'Guidance',
   Lost:'Guidance',Misunderstood:'Humility','Feeling superior':'Humility',Compared:'Identity',
   Fooled:'Identity',Inadequate:'Identity',Shame:'Identity',Bored:'Inspiration',Uninspired:'Inspiration',
   Abused:'Justice',Injustice:'Justice',Used:'Justice',Betrayed:'Loyalty',Controlled:'Freedom',
   'Slowed down':'Freedom',Limited:'Freedom',Manipulated:'Freedom',Pressured:'Freedom',Imprisoned:'Freedom',
   Impatient :'Patience',Hurried:'Patience',Vengeful:'Forgiveness',Resentful:'Forgiveness',
   Overwhelmed:'Perseverance',Defeated:'Perseverance',Unprotected:'Protection',Defenseless:'Protection',
   Vulnerable:'Protection','Held accountable':'Responsibility',Dependent:'Responsibility',Confused:'Wisdom',
   Frightened:'Courage',Insecure:'Courage',Frozen:'Courage'}}
 launchEmotionModal=(id)=>{document.getElementById('emotionModal'+id).classList.add('is-active')}
 selectedEmotions = (emotions) => {return this.setState({selectedEmotions:emotions})};
 tableSubmit=()=>{let emotions = this.state.selectedEmotions 
    let virtuesToDevelop = emotions.map(emotion=> this.state.emotionsToVirtuesMap[emotion]); 
    this.setState({hasResult:true, virtuesToDevelop:virtuesToDevelop});  
    return this.props.setResult(virtuesToDevelop)};  
 render(){const tableButtons =  <tr><td colSpan="6"><button className="button is-medium is-pulled-right" 
   style={{backgroundColor:"#870404", color:"#FFF", fontSize:'1rem'}} onClick={this.tableSubmit}>Save</button>
  <button className="button is-medium is-pulled-right" 
   style={{marginLeft:'10px',marginRight:'10px', fontSize:'1rem'}}>Cancel</button></td></tr>
  const tableResult = <tr><td colSpan="6">{this.state.virtuesToDevelop.map(virtue=><p>{virtue}</p>)}</td></tr>
  const selectedEmotions = <div>{this.state.selectedEmotions.map(emotion=>
   <span className="tag is-primary is-medium" style={{marginLeft:'10px',marginTop:'5px',
    marginBottom:'5px'}}>{emotion}</span>)}</div>
  return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
  <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
   <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">To be calm or when I feel bad I need-I hurt myself when I can not control:</p></th>
   <th style={{backgroundColor:'#DDD', width:'15%'}}><p className="subtitle is-6">Frequency-Every:</p></th>
   <th style={{backgroundColor:'#DDD', width:'15%'}}>
    <p className="subtitle is-6">Incorrect reason (Negative emotion)</p></th>
   <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">Wrong way:</p></th>
   <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">Incorrect amount:</p></th>
   <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">Incorrect moment:</p></th></tr></thead>
  <tbody><tr><td><input className="input" type="text"/></td>
   <td><div className="select" style={{width:'100%'}}><select style={{width:'100%'}}>
    <option selected hidden>Choose</option><option>Hourly</option><option>Daily</option>
    <option>Weekly</option><option>Other</option></select></div></td>
   <td style={{paddingTop:'15px'}} className="has-text-centered">{selectedEmotions}
   <a onClick={()=>this.launchEmotionModal(this.props.id )}>Add an emotion</a></td>
   <td><input className="input" type="text"/></td><td><input className="input" type="text"/></td>
   <td><input className="input" type="text"/></td></tr>
   {this.state.hasResult?tableResult:tableButtons}</tbody></table></div>
    <EmotionModal id={this.props.id} selectedEmotions={this.selectedEmotions} modalClass="modal"/></div>)}}
export default TableActivity6;
