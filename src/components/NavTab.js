import React from 'react';

const NavTab = () => (
<div className="columns has-background-white;" style={{marginBottom:'0%', marginLeft:'5%', marginRight:'5%'}}>
 <div className="column is-7" style={{paddingBottom:'0px'}}><nav className="breadcrumb is-medium" aria-label="breadcrumbs"><ul>
  <li><a href="#" className="has-text-dark" style={{paddingTop:'8px'}}><b>Plan V</b></a></li></ul></nav></div> 
   <div className="column is-5" style={{paddingBottom:'0px'}}>
 <div className="tabs is-centered is-fullwidth"><ul style={{borderBottomStyle:'solid'}}>
  <li><a className="has-text-dark">My Company</a></li><li><a className="has-text-dark">My Code</a></li>
  <li className="has-text-dark"><a className="has-text-dark">My Virtues</a></li>
  <li><a className="has-text-dark">Tutorial</a></li></ul></div></div></div>
);

export default NavTab;
