import React,{Component} from 'react';  class TableActivity7 extends Component{state={}
 virtueValue(virtue){const sumValue = this.props.areas.map(area=>document.getElementById(area+virtue).value===''
   ?0:parseFloat(document.getElementById(area+virtue).value)).reduce((a,b) => a + b, 0); 
  const lenValue = this.props.areas.map(area=>document.getElementById(area+virtue).value===''?0:1)
   .reduce((a,b) => a + b, 0);
  const maxValue = this.props.areas.map(area=>document.getElementById(area+virtue).value===''?0
   :parseFloat(document.getElementById(area+virtue).value)).reduce((a,b) => Math.max(a,b), 0);
  document.getElementById('sufficiency'+virtue).innerHTML = Math.round((((sumValue/lenValue)+maxValue)/2)*10)/10}
 tableheaders(){const row =this.props.areas.map(area=>{return(<th style={{verticalAlign:'middle',textAlign:'center'}}
   key={area}>{area}</th>)}); return <tr><th></th>{row}
  <th style={{verticalAlign:'middle',textAlign:'center'}}>Sufficiency Index</th></tr>}
 tablerows=()=>{ const rows = this.props.virtues.map(virtue=> {const inputs = this.props.areas.map(
   area =>{return(<td key={area+virtue}><input className="input" type="number" 
   onChange={(e)=>{this.virtueValue(virtue)}} max="3" min="1" step="0.2" id={area+virtue}/></td>)});
  return (<tr key={virtue}><td>{virtue}</td>{inputs}
   <td id={'sufficiency'+virtue} style={{textAlign:'center'}}></td></tr>) }); return <tbody>{rows}</tbody>}
 render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}} id={this.props.id}>
  <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead>
   <tr><th style={{backgroundColor:'#335e92BB', textAlign:'center'}} colSpan={this.props.areas.length + 2}>
    <p className="subtitle is-5" style={{color:'#FFF'}}>{this.props.title}</p></th></tr>{this.tableheaders()}</thead>
  {this.tablerows()}</table></div></div>)}}
  export default TableActivity7;
