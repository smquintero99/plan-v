import React from 'react'; import {Link} from 'react-router-dom';const Carousel = () => (
<div className="columns"  style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}><div className="column">
 <div className='carousel carousel-animated carousel-animate-slide' data-size="3"><div className='carousel-container'>
 <div className='carousel-item has-background is-active'><figure className="image is-5by4">
  <img className="is-background" src="https://firebasestorage.googleapis.com/v0/b/planv-aa770.appspot.com/o/portada1.jpg?alt=media&token=0de58ea4-68e6-4f2d-ab83-97ca7ccdf62f" alt="" style={{borderRadius:'10px'}}/></figure>
  <div className="title">Leadership Based on Virtues</div></div>
 <div className='carousel-item has-background is-active'><Link to="/module/1"><figure className="image is-5by4">
  <img className="is-background" src="https://firebasestorage.googleapis.com/v0/b/planv-aa770.appspot.com/o/module1.jpg?alt=media&token=7e71f6d4-c3cf-4e0e-b74e-cad88247fc90" alt="" style={{borderRadius:'10px'}}/></figure>
  <div className="title">Module 1: Your Right Reasons</div></Link></div>
 <div className='carousel-item has-background'><Link to="/module/2"><figure className="image is-5by4">
  <img className="is-background" src="https://firebasestorage.googleapis.com/v0/b/planv-aa770.appspot.com/o/module2.jpg?alt=media&token=ebbe3c58-941f-4d10-90c5-97f901464d85" alt="" style={{borderRadius:'10px'}}/></figure>
  <div className="title">Module 2: Personal Code of Conduct</div></Link></div>
 <div className='carousel-item has-background'><Link to="/module/3"><figure className="image is-5by4">
  <img className="is-background" src="https://firebasestorage.googleapis.com/v0/b/planv-aa770.appspot.com/o/module3.jpg?alt=media&token=a518ed7a-7f7f-468e-8ef5-a890e80839d5" alt="" style={{borderRadius:'10px'}}/></figure>
  <div className="title">Module 3: Personal Life-Project</div></Link></div>
 <div className='carousel-item has-background'><Link to="/module/4"><figure className="image is-5by4">
  <img className="is-background" src="https://firebasestorage.googleapis.com/v0/b/planv-aa770.appspot.com/o/module4.jpg?alt=media&token=e776e692-715d-4699-b23f-6be99b3f09fa" alt="" style={{borderRadius:'10px'}}/></figure>
  <div className="title">Module 4: Personal Life-Purpose</div></Link></div>
 <div className='carousel-item has-background'><Link to="/module/5"><figure className="image is-5by4">
  <img className="is-background" src="https://firebasestorage.googleapis.com/v0/b/planv-aa770.appspot.com/o/module5.jpg?alt=media&token=ce83a2db-b375-452a-bbd5-185d7b2a9333" alt="" style={{borderRadius:'10px'}}/></figure>
  <div className="title">Module 5: Virtues and Professional Success</div></Link></div>
 <div className='carousel-item has-background'><Link to="/module/6"><figure className="image is-5by4">
  <img className="is-background" src="https://firebasestorage.googleapis.com/v0/b/planv-aa770.appspot.com/o/module6.png?alt=media&token=0ca8920d-da92-4666-9f1d-2d64735156e0" alt="" style={{borderRadius:'10px'}}/></figure>
  <div className="title">Module 6: Virtues and Your Life Mate</div></Link></div></div>
 <div className="carousel-navigation"><div className="carousel-nav-left"><i className="fa fa-chevron-left" aria-hidden="true"></i></div>
 <div className="carousel-nav-right"><i className="fa fa-chevron-right" aria-hidden="true"></i></div></div></div> </div></div>
  ); export default Carousel;
