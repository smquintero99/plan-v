import React,{Component} from 'react'; class InfoCard extends Component{render(){return(
<div className="columns"  style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}><div className="column is-12">
 <div className="card" style={{backgroundColor:'#335e92'}}><div className="card-content">
  <p className="subtitle is-6 has-text-white">Evaluate each dimension of your life with the following scale: 
  <b>1</b> = very good, <b>1.5</b> =good, <b>2</b> = fair, <b>2.5</b> = bad, <b>3</b> = very bad.</p>
 <p className="title is-5 has-text-white">The smaller the number, the better quality of life you have.</p></div></div></div></div>
)}};export default InfoCard;
