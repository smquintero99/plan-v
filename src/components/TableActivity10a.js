import React,{Component} from 'react'; 
class TableActivity10a extends Component{tablerows(){return this.props.activities.map(activity=>{ 
 return <tr><td><p className="title is-6" style={{marginTop:'10px'}}>{activity.virtue}</p>
   <p className="subtitle is-6" style={{marginTop:'-10px'}}>{activity.meaning}</p></td>
  <td style={{width:'10%'}}><input style={{marginTop:'20px'}} className="input" type="number"
   max="3" min="1" step="0.2"/></td></tr>})}
render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
 <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
  <th style={{backgroundColor:'#DDD'}} colSpan={2}>
   <p className="title is-5" style={{color:'#335e92'}}>{this.props.header}</p></th></tr></thead>
  <tbody>{this.tablerows()}</tbody></table></div></div>)}}export default TableActivity10a;

