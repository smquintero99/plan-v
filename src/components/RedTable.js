import React,{Component} from 'react'; class RedTable extends Component{render(){
const {activities}=this.props; const activityList = activities.map(activity=>{return(<tr>
 <th style={{verticalAlign:'middle', backgroundColor:'#870404'}}>
  <p className="subtitle is-4" style={{color:'#FFF', margin:'5px'}}>{activity.index}</p></th><td>{activity.text}</td></tr>)});
return(<div className="columns"  style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}><div className="column is-12">
 <table className="table is-bordered" style={{width:'100%'}}><thead><tr><th colSpan="2">
  <p className="title is-4">{this.props.title}</p></th></tr></thead>
  <tbody>{activityList}<tr style={{backgroundColor:'#DDD'}}><td></td><td><div className="is-pulled-right">
   <div className="field is-horizontal"><div className="field-label" style={{flexGrow:'3', verticalAlign:'middle'}}>
    <label className="label" style={{marginTop:'8px'}}>{this.props.footer}</label></div>
    <div className="field-body" style={{maxWidth:'10%'}}><div className="field"><p className="control">
     <input className="input is-info" type="text" id={'index' + this.props.title}/></p></div></div></div></div></td></tr></tbody></table></div></div>
)}};export default RedTable;
