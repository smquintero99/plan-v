import React,{Component} from 'react';  import CheckBoxModalTable from '../components/CheckBoxModalTable';
class EmotionModal extends Component{
 state={emotions:[['Disappointed','Deceived','Frustrated','Envious'],['Sad','Melancholic','Crowded','Invaded'],
   ['Grossed out','Guilty','Hurt','Ignored'],['Incompetent','Insufficient','Judged','Despised'],
   ['Lazy','Procrastinating','Scarcity','Robed'],['Disoriented','Lost','Misunderstood','Feeling superior'],
   ['Compared','Fooled','Inadequate','Shame'],['Bored','Uninspired','Abused','Injustice'],
   ['Used','Betrayed','Controlled','Slowed down'],['Limited','Manipulated','Pressured','Imprisoned'],
   ['Impatient','Hurried','Vengeful','Resentful'],['Overwhelmed','Defeated','Unprotected','Defenseless'],
   ['Vulnerable','Held accountable','Dependent','Confused'],['Frightened','Insecure','Frozen','']]};
 saveEmotionModal=()=>{let selectedEmotions= this.state.emotions.flat().filter(emotion=> emotion!== '')
  .filter(emotion=> document.getElementById(emotion+this.props.id).checked===true); this.props.selectedEmotions(selectedEmotions);
  document.getElementById('emotionModal'+this.props.id).classList.remove('is-active')};
 dismissEmotionModal=()=>{ document.getElementById('emotionModal'+this.props.id).classList.remove('is-active')}
 render(){return(<div className={this.props.modalClass} id={'emotionModal'+this.props.id}><div className="modal-background"></div>
 <div className="modal-card" style={{width:'80%'}}><header className="modal-card-head">
 <p className="modal-card-title">Select one or more emotions</p>
 <button className="delete" aria-label="close" onClick={this.dismissEmotionModal}></button></header>
 <section className="modal-card-body"><CheckBoxModalTable id={this.props.id} title="" activities={this.state.emotions}/></section>
 <footer className="modal-card-foot"><div style={{width:'100%'}}>
 <button onClick={this.saveEmotionModal} className="button is-success is-pulled-right" style={{marginLeft:'3%'}}>Save changes</button>
 <button onClick={this.dismissEmotionModal} className="button is-pulled-right">Cancel</button></div></footer></div></div>)}}
 export default EmotionModal;

