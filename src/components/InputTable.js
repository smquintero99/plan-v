import React,{Component} from 'react'; class CheckBoxTable extends Component{
 tablerows(){return this.props.activities.map(rows=>{var row=rows.map(cell=>{if (cell!=''){return <th style={{verticalAlign:'middle'}}><label className="subtitle is-6"><input type="checkbox" style={{marginRight:'9px'}} />{cell}</label></th>}else{return <th></th>}});return <tr>{row}</tr>})}
 render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}><div className="column is-12">
  <table className="table is-bordered" style={{width:'100%'}}><thead><tr><th colSpan="6" style={{backgroundColor:'#DDD'}}>
   <p className="title is-6">{this.props.title}</p></th></tr></thead>
  <tbody>{this.tablerows()}</tbody></table></div></div>)}}export default CheckBoxTable;

