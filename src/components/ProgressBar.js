import React from 'react';

const ProgressBar = () => (

 <div><div className="columns has-background-white;" style={{marginTop:'2%', marginBottom:'2%', marginLeft:'5%', marginRight:'5%'}}>
  <div className="column is-12"><ul className="steps"><li className="step-item is-completed is-warning">
  <div className="step-marker has-text-grey-dark">1</div>
  <div className="step-details is-warning is-completed"><p className="step-title has-text-grey-dark">Module 1</p></div></li>
   <li className="step-item is-warning is-active"><div className="step-marker has-text-grey-dark">2</div>
  <div className="step-details is-warning"><p className="step-title has-text-grey-dark">Module 2</p></div></li>
   <li className="step-item"><div className="step-marker">3</div><div className="step-details">
    <p className="step-title has-text-grey">Module 3</p></div></li>
   <li className="step-item"><div className="step-marker">4</div><div className="step-details">
    <p className="step-title has-text-grey">Module 4</p></div></li>
   <li className="step-item"><div className="step-marker">5</div><div className="step-details">
    <p className="step-title has-text-grey">Module 5</p></div></li>
   <li className="step-item"><div className="step-marker">6</div><div className="step-details">
    <p className="step-title has-text-grey">Module 6</p></div></li></ul></div></div></div>
);

export default ProgressBar;
