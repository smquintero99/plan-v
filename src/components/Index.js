import React,{Component} from 'react'; import {BrowserRouter as Router,Route,Link,Switch,Redirect} from 'react-router-dom';
const indexTableStyle={padding: '1em .75em'}; class Index extends Component{render(){const {activities}=this.props; const activityList = activities.map(activity=>{return(<tr><td style={indexTableStyle}><Link to={activity.link}>{activity.name}</Link></td></tr>)});return(
<div className="columns"  style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}><div className="column is-12">
 <table className="table" style={{width:'90%', marginLeft:'5%', marginTop:'2%'}}><thead><tr>
  <th style={{backgroundColor:'#DDD', width:'100%', padding: '1em .75em'}}><p className="subtitle is-6">{this.props.title}</p></th></tr></thead><tbody>{activityList}</tbody></table></div></div>)}};export default Index;
