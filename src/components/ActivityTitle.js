import React,{Component} from 'react'; class BreadCrumb extends Component{render(){return(
<div className="columns has-background-white;" style={{marginLeft:'5%', marginRight:'5%', marginTop:'1%', marginBottom:'3%'}}>
 <div className="column is-12"><h1 className="title is-4" style={{color:'#335e92'}}>{this.props.title}</h1></div></div>
)}};export default BreadCrumb;
