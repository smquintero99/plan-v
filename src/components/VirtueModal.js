import React,{Component} from 'react';  import CheckBoxModalTable from '../components/CheckBoxModalTable';
class VirtueModal extends Component{state={virtues:[['Acceptance','Compassion','Generosity'],
 ['Inspiration','Patience','Responsibility'],['Admiration','Dignity','Guidance'],['Justice','Forgiveness','Wisdom'],
 ['Joyfulness','Discipline','Humility'],['Loyalty','Perseverance','Courage'],['Companionship','Respect','Identity'],
 ['Freedom','Protection','']]} 
 dismissVirtueModal(){document.getElementById('virtueModal'+this.props.id).classList.remove('is-active')};
 saveVirtueModal=()=>{let selectedVirtues= this.state.virtues.flat().filter(virtue=> virtue !== '')
  .filter(virtue=> document.getElementById(virtue+this.props.id).checked===true); 
  this.props.selectedVirtues(selectedVirtues);
  document.getElementById('virtueModal'+this.props.id).classList.remove('is-active')};
 render(){return(<div className={this.props.modalClass} id={"virtueModal"+this.props.id}>
  <div className="modal-background"></div>
  <div className="modal-card" style={{width:'80%'}}><header className="modal-card-head">
   <p className="modal-card-title">Select one or more virtues</p>
   <button className="delete" aria-label="close" onClick={this.dismissVirtueModal}></button></header>
   <section className="modal-card-body"><CheckBoxModalTable id={this.props.id} title="" 
    activities={this.state.virtues}/></section>
   <footer className="modal-card-foot"><div style={{width:'100%'}}>
   <button onClick={this.saveVirtueModal} className="button is-success is-pulled-right" style={{marginLeft:'3%'}}>
    Save changes</button><button onClick={this.dismissVirtueModal} className="button is-pulled-right">Cancel</button>
  </div></footer></div></div>)}} export default VirtueModal;
 
