import React,{Component} from 'react'; import VirtueModal from '../components/VirtueModal'; class TableActivity8 extends Component{
tablerows(){return this.props.activities.map(activity=>{ return <tr><td>
 <p className="subtitle is-6" style={{marginTop:'10px'}}>{activity}</p></td><td><input className="input" type="text"/></td></tr>})}
render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
 <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
  <th style={{backgroundColor:'#DDD', width:'25%'}}><p className="subtitle is-6">{this.props.header1}</p></th>
  <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">Your Code of Conduct</p></th></tr></thead>
  <tbody>{this.tablerows()}</tbody></table></div><VirtueModal modalClass="modal"/>
  </div>)}}export default TableActivity8;
