import React,{Component} from 'react'; 
class TableActivity10b extends Component{tablerows(){return this.props.activities.map(activity=>{ 
 return <tr><td><p className="title is-6" style={{marginTop:'10px'}}>{activity.virtue}</p></td>
  <td style={{width:'10%', textAlign:'center'}}>{activity.result}</td></tr>})}
render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
 <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
  <th style={{backgroundColor:'#335e92'}} colSpan={2}>
   <p className="title is-5" style={{color:'#DDD'}}>{this.props.header}</p></th></tr></thead>
  <tbody>{this.tablerows()}</tbody></table></div></div>)}}export default TableActivity10b;

