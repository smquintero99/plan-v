import React,{Component} from 'react'; import EmotionModal from '../components/EmotionModal'; 
class TableActivity4 extends Component{state={selectedEmotions:[], virtuesToDevelop:[],
  emotionsToVirtuesMap:{Disappointed:'Acceptance',Deceived:'Acceptance',Frustrated:'Acceptance',Envious:'Admiration',
   Sad:'Joyfulness',Melancholic:'Joyfulness',Crowded:'Companionship',Invaded:'Companionship','Grossed out':'Compassion',
   Guilty:'Dignity',Hurt:'Dignity',Ignored:'Dignity',Incompetent:'Dignity',Insufficient:'Dignity',Judged:'Dignity',
   Despised:'Dignity',Lazy:'Discipline',Procrastinating:'Discipline',Scarcity:'Generosity',Robed:'Generosity',
   Disoriented:'Guidance',Lost:'Guidance',Misunderstood:'Humility','Feeling superior':'Humility',Compared:'Identity',
   Fooled:'Identity',Inadequate:'Identity',Shame:'Identity',Bored:'Inspiration',Uninspired:'Inspiration',Abused:'Justice',
   Injustice:'Justice',Used:'Justice',Betrayed:'Loyalty',Controlled:'Freedom','Slowed down':'Freedom',Limited:'Freedom',
   Manipulated:'Freedom',Pressured:'Freedom',Imprisoned:'Freedom',Impatient :'Patience',Hurried:'Patience',
   Vengeful:'Forgiveness',Resentful:'Forgiveness',Overwhelmed:'Perseverance',Defeated:'Perseverance',Unprotected:'Protection',
   Defenseless:'Protection',Vulnerable:'Protection','Held accountable':'Responsibility',Dependent:'Responsibility',
   Confused:'Wisdom',Frightened:'Courage',Insecure:'Courage',Frozen:'Courage'}};
launchEmotionModal(id){document.getElementById('emotionModal'+id).classList.add('is-active')};
selectedEmotions = (emotions) => {return this.setState({selectedEmotions:emotions})};
tableSubmit=()=>{let emotions = this.state.selectedEmotions; 
  let virtuesToDevelop = emotions.map(emotion=> this.state.emotionsToVirtuesMap[emotion]); 
  this.setState({hasResult:true, virtuesToDevelop:virtuesToDevelop}); return this.props.setResult(virtuesToDevelop)};
render(){const tableResult = <tr><td colSpan="5">{this.state.virtuesToDevelop.map(virtue=> <p>{virtue}</p>)}</td></tr>
 const tableButtons =  <tr><td colSpan="5">
   <a className="button is-medium is-pulled-right" style={{backgroundColor:"#870404", color:"#FFF", fontSize:'1rem'}} onClick={this.tableSubmit}>Save</a>
   <a className="button is-medium is-pulled-right" style={{marginLeft:'10px',marginRight:'10px', fontSize:'1rem'}}>Cancel</a>
  </td></tr>
  const selectedEmotions = <div>{this.state.selectedEmotions.map(emotion=>
    <span className="tag is-primary is-medium" style={{marginLeft:'10px',marginTop:'5px',marginBottom:'5px'}}>{emotion}</span>)}</div>
 return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}><div className="column is-12">
 <table className="table is-bordered" style={{width:'100%'}}><thead><tr>
  <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">I do not like that my:</p></th>
  <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">Do-Do not, Be-Do not Be:</p></th>
  <th style={{backgroundColor:'#DDD'}}><p className="subtitle is-6">It makes me angry because it makes me feel:</p></th></tr></thead>
  <tbody><tr><td><div className="select"><select><option selected hidden>Choose</option>
    <option>Couple</option><option>Son</option><option>Father</option><option>Mother</option><option>Brother</option>
    <option>Grandfather</option><option>Uncle</option><option>Cousin</option><option>Friend</option>
    <option>Boss</option><option>Collaborator</option><option>Partner</option><option>Ex partner</option>
    <option>Employee</option><option>People</option><option>Customers</option><option>Myself</option></select></div></td>
   <td><input className="input" type="text"/></td>
   <td style={{paddingTop:'15px'}} className="has-text-centered">{selectedEmotions}<a onClick={()=>this.launchEmotionModal(this.props.id)}>Add an emotion</a></td>     
  </tr>{this.state.hasResult?tableResult:tableButtons}</tbody></table></div>
  <EmotionModal id={this.props.id} selectedEmotions={this.selectedEmotions} modalClass="modal"/></div>)}}export default TableActivity4;

