import React,{Component} from 'react'; import VirtueModal from '../components/VirtueModal'; class TableActivity11 extends Component{
 tablerows(){return this.props.activities.map(activity=>{ 
   return <span class="tag is-primary is-medium" style={{marginLeft:'10px',marginTop:'5px',marginBottom:'5px'}}>{activity}</span>})}
 tablerows1(){return this.props.activities1.map(activity=>{ return <span class="tag is-primary is-medium" style={{marginLeft:'10px',marginTop:'5px',marginBottom:'5px'}}>{activity}</span>})}
 render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
  <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
    <th style={{backgroundColor:'#DDD'}}>
     <p className="title is-6" style={{color:'#335e92'}}>Elements:</p></th>
    <th style={{backgroundColor:'#DDD'}}>
     <p className="title is-6" style={{color:'#335e92'}}>I am here to grow and help others grow:</p></th></tr></thead>
   <tbody><tr><td>Developing the virtues to the maximum:</td><td>{this.tablerows()}</td></tr>
    <tr><td>Applying my virtues of:</td><td>{this.tablerows1()}</td></tr>
    <tr><td>Trying to improve the world of:</td><td><input className="input" type="text"/></td></tr>
    <tr><td>Through create:</td><td><input className="input" type="text"/></td></tr>
    <tr><td>With the hope that it lasts:</td><td>
     <div className="select" style={{width:'100%'}}><select style={{width:'100%'}}><option selected hidden>Choose</option>
     <option>While I can control it</option><option>As long as I can influence it</option><option>While I live</option>
     <option>As long as my closest friends live</option><option>{"As long as everyone I've met"}</option>
     <option>{"While the ones I've impacted live"}</option></select></div></td></tr>
   </tbody></table></div></div>)}}
export default TableActivity11;

