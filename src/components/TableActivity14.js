import React,{Component} from 'react'; import 'bulma-switch/dist/css/bulma-switch.min.css'
import VirtueModal from '../components/VirtueModal'; class TableActivity14 extends Component{
 state=this.createStateForSwitches()
 createStateForSwitches(){let dict={}; this.props.activities.forEach(activity=>{dict[activity+1]=false; dict[activity+2]=false; dict[activity+3]=false}); return dict}
 setLegend(activity){let condition1 = this.state[activity.toString()+1]; let condition2 = this.state[activity.toString()+2] 
  let condition3 = this.state[activity.toString()+3]; if(condition1 == true && condition2 == true && condition3 == true){return 'Great appreciation and admiration for the company. Recognition of personal growth.'}else if(condition1 == true && condition2 == true && condition3 == false){return 'Great appreciation and admiration for the company. Challenge of personal growth.'}else if(condition1 == true && condition2 == false && condition3 == false){return 'High risk of mutual disenchantment between company and collaborator (a).'}else if(condition1 == true && condition2 == false && condition3 == true){return 'This virtue in sufficiency allows a high level of personal effectiveness in the company.'}else if(condition1 == false && condition3 == true){return 'This virtue in sufficiency allows a good level of personal effectiveness in the company.'}else if(condition1 == false && condition3 == false){return 'Develop this virtue quickly to increase the effectiveness of the collaborator.'}}
 switchState = e =>{let newState = !this.state[e.target.id]; this.setState({[e.target.id]:newState}); console.log(this.state)}
 displaySwitch2(activity){if(this.state[activity.toString()+1]==false){return true}else{return false}}
 tablerows(){console.log(this.state); return this.props.activities.map(activity=>{ return <tr><td>{activity}</td><td>
<div className="field" style={{textAlign:'center'}}><input id={activity + '1'} type="checkbox" name="switchExample" className="switch" checked={this.state[activity.toString()+1]}/>
  <label id={activity+1} htmlFor={activity + '1'} onClick={this.switchState}></label></div></td><td>
<div className="field" style={{textAlign:'center'}} hidden={this.displaySwitch2(activity)}><input id={activity + '2'} type="checkbox" name="switchExample" className="switch" checked={this.state[activity.toString()+2]}/>
  <label id={activity+2} htmlFor={activity + '2'}  onClick={this.switchState}></label></div></td><td>
<div className="field" style={{textAlign:'center'}}><input id={activity + '3'} type="checkbox" name="switchExample" className="switch" checked={this.state[activity.toString()+3]}/>
  <label id={activity+3} htmlFor={activity + '3'} onClick={this.switchState}></label></div></td><td>{this.setLegend(activity)}
</td></tr>})}
 render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
  <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
    <th style={{backgroundColor:'#DDD'}}>
     <p className="title is-6" style={{color:'#335e92'}}>My virtues to develop:</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Does my company have this virtue developed to excellence?</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Have I advanced in the development of this virtue thanks to my presence in the company?</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Thanks to my work or other life experiences, have I developed this virtue at a sufficient level?</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Legend</p></th></tr></thead>
   <tbody>{this.tablerows()}</tbody></table></div></div>)}}
export default TableActivity14;

