import React,{Component} from 'react'; class CheckBoxModalTable extends Component{
 tablerows(){return this.props.activities.map(rows=>{var row=rows.map(cell=>{if (cell!==''){
     return <th style={{verticalAlign:'middle'}}><label className="subtitle is-6">
     <input type="checkbox" style={{marginRight:'9px'}} id={cell+this.props.id}/>{cell}</label></th>}else{return <th></th>}});
     return <tr height='50px'>{row}</tr>})}
 render(){return(<div className="columns"><div className="column is-12">
  <table className="table is-bordered" style={{width:'100%'}}>
  <tbody>{this.tablerows()}</tbody></table></div></div>)}}export default CheckBoxModalTable;

