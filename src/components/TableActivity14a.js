import React,{Component} from 'react'; import 'bulma-switch/dist/css/bulma-switch.min.css'
import VirtueModal from '../components/VirtueModal'; class TableActivity14a extends Component{state=this.createStateForSwitches()
 createStateForSwitches(){let dict={}; this.props.activities.forEach(activity=>{dict[activity+1]=false; dict[activity+2]=false; dict[activity+3]=false}); return dict}
 setLegend(activity){let condition1=this.state[activity.toString()+1]; let condition2=this.state[activity.toString()+2]; 
  let condition3=this.state[activity.toString()+3]; if(condition1==true){return 'Virtues in common create a strong sense of belonging and loyalty to the company.'}else if(condition1==false && condition2==false){return 'This virtue in sufficiency allows the company to provide a healthy working relationship.'}else if(condition1==false && condition2 == true && condition3 == false){return 'Try to develop this virtue in the company. Individual contribution to culture.'}else if(condition1==false && condition2==true && condition3==true){return 'Great recognition of this virtue by the company to the collaborator (a).'}}
 switchState = e =>{let newState = !this.state[e.target.id]; this.setState({[e.target.id]:newState}); console.log(this.state)}
 displaySwitch2(activity){if(this.state[activity.toString()+1]==true){return true}else{return false}}; displaySwitch3(activity){if(this.state[activity.toString()+1]==false && this.state[activity.toString()+2]==true){return false}else{return true}}
 tablerows(){console.log(this.state); return this.props.activities.map(activity=>{ return <tr><td>{activity}</td><td>
<div className="field" style={{textAlign:'center'}}><input id={activity + '1'} type="checkbox" name="switchExample" className="switch" checked={this.state[activity.toString()+1]}/>
  <label id={activity+1} htmlFor={activity + '1'} onClick={this.switchState}></label></div></td><td>
<div className="field" style={{textAlign:'center'}} hidden={this.displaySwitch2(activity)}><input id={activity + '2'} type="checkbox" name="switchExample" className="switch" checked={this.state[activity.toString()+2]}/>
  <label id={activity+2} htmlFor={activity + '2'}  onClick={this.switchState}></label></div></td><td>
<div className="field" style={{textAlign:'center'}} hidden={this.displaySwitch3(activity)}><input id={activity + '3'} type="checkbox" name="switchExample" className="switch" checked={this.state[activity.toString()+3]}/>
  <label id={activity+3} htmlFor={activity + '3'} onClick={this.switchState}></label></div></td><td>{this.setLegend(activity)}
</td></tr>})}
 render(){return(<div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
  <div className="column is-12"><table className="table is-bordered" style={{width:'100%'}}><thead><tr>
    <th style={{backgroundColor:'#DDD'}}>
     <p className="title is-6" style={{color:'#335e92'}}>My developed virtues:</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Does my company have this virtue developed to excellence?</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>When I arrived, my company had this virtue as a lack:</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Has my company developed this virtue at a sufficient level?</p></th>
    <th style={{backgroundColor:'#DDD'}}><p className="title is-6" style={{color:'#335e92'}}>Legend</p></th></tr></thead>
   <tbody>{this.tablerows()}</tbody></table></div></div>)}}
export default TableActivity14a;

