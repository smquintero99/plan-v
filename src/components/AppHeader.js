import React from 'react';const AppHeader = () => (
<nav className="navbar is-black" style={{height:"120px", backgroundColor:"#000"}}><div className="navbar-brand" style={{maxHeight:"auto", marginTop:"25px", height:"90px",  marginLeft:"5%"}}><a className="navbar-item" href="#">
 <img src="https://firebasestorage.googleapis.com/v0/b/planv-aa770.appspot.com/o/planvlogo.png?alt=media&token=6d7066ae-dc6c-4215-a1f5-8a06bcd0a90c" alt="Bulma: a modern CSS framework based on Flexbox" style={{width:"auto", height:"61px"}}/></a>
 <div className="navbar-burger burger" data-target="navbarExampleTransparentExample"><span></span><span></span>
  <span></span></div></div><div id="navbarExampleTransparentExample" className="navbar-menu">
  <div className="navbar-end" style={{marginTop:'50px', marginRight:'6%'}}><a className="navbar-item" href="#">Products</a>
   <a className="navbar-item" href="#">Contact</a><div className="navbar-item has-dropdown is-hoverable">
    <a className="navbar-link" href="#">Options</a><div className="navbar-dropdown is-boxed">
     <a className="navbar-item" href="#">Profile</a><a className="navbar-item" href="#">Edit Profile</a>
     <a className="navbar-item is-active" href="#">Log Out</a></div></div><div className="navbar-item"><div className="field is-grouped">
     <p className="control"><a className="button is-black" style={{backgroundColor:'#000'}}><span>Welcome Santiago</span></a></p>
 </div></div></div></div></nav>
);export default AppHeader;
