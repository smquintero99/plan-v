import React, { Component } from 'react'; import NavTab from '../components/NavTab';
import BreadCrumb from '../components/BreadCrumb'; import SimpleInfoCard from '../components/SimpleInfoCard'; 
import ActivityTitle from '../components/ActivityTitle'; import TableActivity9 from '../components/TableActivity9'; 
import {Link} from 'react-router-dom';  class Activity3 extends Component{render(){return(
 <div style={{marginBottom:"100px"}}><BreadCrumb active="Exercise 9. Ideal Life Project"/><NavTab/>
 <SimpleInfoCard cardClass="column is-12" text="Create a new project each week, based on last week's."/>
 <ActivityTitle title="Virtues To Develop" />
 <TableActivity9 virtue="Dignity" subtitle="It develops: Give gifts of personal taste to one (a)."/>
 <TableActivity9 virtue="Inspiration" subtitle="It develops: Generate changes in routines."/>
 <TableActivity9 virtue="Loyalty" subtitle="It develops: Choose and act in line with a voluntary commitment."/>
 <TableActivity9 virtue="Protection" subtitle="It develops: Choose and eliminate risks for one (a)."/>
 <TableActivity9 virtue="Identity" subtitle="It develops: Perform actions of personal taste while people see us."/>
 <TableActivity9 virtue="Wisdom" subtitle="It develops: Investigating something that makes us curious."/>
 <TableActivity9 virtue="Company" subtitle="It develops: Spending time in silence with someone you love."/>
 <TableActivity9 virtue="Perseverance" subtitle="It develops: Place small obstacles and overcome them."/>
 <TableActivity9 virtue="Admiration" subtitle="It develops: Using a creation of others or create something similar."/>
 <TableActivity9 virtue="Forgiveness" subtitle="Se desarrolla: Correr riesgos incrementales para lograr lo que se quiere."/>
 <TableActivity9 virtue="Freedom" subtitle='Developed: Replace "I have to do" with "I decide to do".'/>
 <TableActivity9 virtue="Discipline" subtitle="It develops: Doing something that is required at the beginning of the day."/>
 <ActivityTitle title="Developed Virtues" />
 <TableActivity9 virtue="Patience" subtitle="It develops: Do a little something that we know will not end soon."/>
 <TableActivity9 virtue="Acceptance" subtitle="It develops: Accepting something you do not like about someone you love or something valuable."/>
 <TableActivity9 virtue="Justice" subtitle="It develops: Make someone reach the product of their effort.."/>
 <div className="columns"><div className="column-is-12" style={{marginLeft:"auto", marginRight:"auto"}}>
  <button className="button is-medium is-pulled-right" 
   style={{marginTop:"25px", backgroundColor:"#870404", color:"#FFF"}}>Save Changes</button></div></div>
 <div className="columns"><div className="column-is-12" style={{marginLeft:"auto", marginRight:"auto"}}>
  <Link to="/module/3"><button className="button is-medium is-pulled-right" 
   style={{marginTop:"25px", backgroundColor:"#000000E8", color:"#FFF"}}>Finalize</button></Link>
 </div></div></div>)}};
export default Activity3;

