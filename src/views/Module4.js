import React, { Component } from 'react';import NavTab from '../components/NavTab';import BreadCrumb from '../components/BreadCrumb'; 
import Index from '../components/Index';
class Module4 extends Component{state={activities:[{name:'Exercise 10. Quality of life index - stress',link:'/activity/10',id:1},{name:'Exercise 11. My Purpose of Life',link:'/activity/11',id:2}]};
render(){return(<div><BreadCrumb active="Module 4"/><NavTab/>
 <Index title="Purpose of Individual Life" activities={this.state.activities}/></div>)}};export default Module4;
