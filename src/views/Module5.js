import React, { Component } from 'react';import NavTab from '../components/NavTab';import BreadCrumb from '../components/BreadCrumb'; 
import Index from '../components/Index';
class Module5 extends Component{state={activities:[{name:'Exercise 14. Purpose of Life and professional success',link:'/activity/14',id:1}]};
render(){return(<div><BreadCrumb active="Module 5"/><NavTab/>
 <Index title="Purpose of Life and professional success" activities={this.state.activities}/></div>)}};export default Module5;
