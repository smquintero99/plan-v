import React, { Component } from 'react';import NavTab from '../components/NavTab'; 
import BreadCrumb from '../components/BreadCrumb'; import ComplexInfoCard from '../components/ComplexInfoCard'; 
import TableActivity4 from '../components/TableActivity4'; import ResultContainer from '../components/resultContainer';
class Activity4 extends Component{state={expectations:'', newExpectations:[], virtuesToDevelop:[], result:false}; 
setResult=(virtuesToDevelop)=>{return this.setState({result:true, virtuesToDevelop:virtuesToDevelop})}
newExpectation=()=>{let newExpectations = this.state.newExpectations; 
  newExpectations.unshift(<TableActivity4 key={newExpectations.length+1} id={newExpectations.length+1}
   setResult={this.setResult}/>);this.setState({expectations:<div>{newExpectations}</div>,newExpectations:newExpectations})}
render(){const resultActivity4 = <div><p className="title is-4 has-text-white">Summary of Virtues in this exercise.</p>
  <table className="table is-bordered" style={{width:"60%", marginLeft:"20%", marginTop:"50px", marginBottom:"50px"}}>
   <thead><tr><th>Virtue</th><th style={{textAlign:"center"}}>No. of appearances</th></tr></thead><tbody>
    {this.state.virtuesToDevelop.map((virtue,i)=> <tr><td>{virtue}</td><td style={{textAlign:"center"}}>1</td></tr>)}</tbody></table></div>
return(<div><BreadCrumb active="Exercise 4. My Expectations from Life"/><NavTab/>
<div className="columns"  style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
<ComplexInfoCard cardClass="column is-10" text="Add one or more expectations you have about people or situations that you do not like. It does not matter that they do not know that you have that expectation about them."/>
<div className="column-is-2" style={{marginLeft:"auto", marginRight:"10px", marginBottom:"10px", marginTop:"auto"}}>
 <a className="button is-medium is-pulled-right" style={{marginTop:"25px", backgroundColor:"#870404", color:"#FFF"}} 
  onClick={this.newExpectation}>New Expectation</a>
</div></div>{this.state.expectations}<TableActivity4 setResult={this.setResult} id="0"/>
{this.state.result ? <ResultContainer content={resultActivity4}/>:null}</div>)}}; export default Activity4;
