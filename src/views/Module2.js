import React, { Component } from 'react';import NavTab from '../components/NavTab';
import BreadCrumb from '../components/BreadCrumb'; import Index from '../components/Index';
class Module2 extends Component{state={activities:[{name:'Exercise 8. Your Personal Code of Conduct',link:'/activity/8',id:8}]};
render(){return(<div><BreadCrumb active="Module 2"/><NavTab/>
 <Index title="Personal Code of Conduct" activities={this.state.activities}/></div>)}}; export default Module2;
