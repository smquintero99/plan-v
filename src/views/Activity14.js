import React, { Component } from 'react';import NavTab from '../components/NavTab';import BreadCrumb from '../components/BreadCrumb'; import SimpleInfoCard from '../components/SimpleInfoCard'; import ActivityTitle from '../components/ActivityTitle';
import TableActivity14 from '../components/TableActivity14';import TableActivity14a from '../components/TableActivity14a';
class Activity14 extends Component{state={activities:['Protection','Identity','Wisdom','Dignity','Discipline','Perseverance', 'Admiration','Company','Inspiration','Loyalty','Forgiveness','Freedom','Courage'],activities1:['Acceptance','Justice','Patience']}
render(){return(<div><BreadCrumb active="Exercise 14. Purpose of Life and professional success"/><NavTab/>
<SimpleInfoCard cardClass="column is-12" text='Answer each question according to the knowledge you have of your company.'/>
 <ActivityTitle title="The virtues that I learn from my company"/>
 <TableActivity14 activities={this.state.activities}/>
 <ActivityTitle title="The virtues that my company learns from me"/>
 <TableActivity14a activities={this.state.activities1}/></div>)}}export default Activity14;
