import React, { Component } from 'react';import NavTab from '../components/NavTab';import BreadCrumb from '../components/BreadCrumb'; import SimpleInfoCard from '../components/SimpleInfoCard'; import ActivityTitle from '../components/ActivityTitle';
import TableActivity11 from '../components/TableActivity11';import TableActivity11a from '../components/TableActivity11a';
class Activity11 extends Component{state={activities:['Protection','Identity','Wisdom','Dignity','Discipline','Perseverance', 'Admiration','Company','Inspiration','Loyalty','Forgiveness','Freedom','Courage'],activities1:['Acceptance','Justice','Patience']}
render(){return(<div><BreadCrumb active="Exercise 11. My Purpose of Life"/><NavTab/>
<SimpleInfoCard cardClass="column is-12" text='Tamaño del Propósito de Vida'/>
 <TableActivity11 activities={this.state.activities} activities1={this.state.activities1} />
 <ActivityTitle title="Attachment to Essential Virtues"/>
 <TableActivity11a activities={this.state.activities} activities1={this.state.activities1} /></div>)}}export default Activity11;
