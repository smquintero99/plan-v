import React, { Component } from 'react';import NavTab from '../components/NavTab';
import BreadCrumb from '../components/BreadCrumb';  import TableActivity7 from '../components/TableActivity7';
import ComplexInfoCard from '../components/ComplexInfoCard'; import {Link} from 'react-router-dom';
class Activity7 extends Component{state={}; render(){return(<div style={{marginBottom:"100px"}}>
  <BreadCrumb active="Exercise 7. Analysis of sufficiency and excellence of virtues."/>
  <NavTab/><div className="columns" style={{marginTop:'0%', marginLeft:'5%', marginRight:'5%'}}>
  <ComplexInfoCard cardClass="column is-12" text="Evaluate each virtue from 1 to 3:  Good -> Bad."/></div>
  <TableActivity7 title='Developing Virtues' areas={['Government','Work team','Workmate','Significant other',
   'Ex significant other','Boss','Son','Other people','Traffic']} virtues={['Dignity','Discipline','Perseverance',
   'Humility','Companionship','Generosity','Compassion']} setResult={''} id="0"/>
  <TableActivity7 title='Born Developed Virtues' areas={['Significant other','Neighbour','Brother','Sister',
   'Broher in law','Ex significant other','Boss','Direct reports','Workmate']} virtues={['Responsability',
   'Guidance','Wisdom','Freedom','Patience','Acceptance','Identity']} setResult={''} id="1"/>
  <div className="columns"><div className="column-is-12" style={{marginLeft:"auto", marginRight:"auto"}}>
   <button className="button is-medium is-pulled-right" 
    style={{marginTop:"25px", backgroundColor:"#870404", color:"#FFF"}}>Save Changes</button></div></div>
  <div className="columns"><div className="column-is-12" style={{marginLeft:"auto", marginRight:"auto"}}>
   <Link to="/module/3"><button className="button is-medium is-pulled-right" 
    style={{marginTop:"25px", backgroundColor:"#000000E8", color:"#FFF"}}>Finalize</button></Link>
  </div></div></div>)}};export default Activity7;
