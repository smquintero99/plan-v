import React, { Component } from 'react';import NavTab from '../components/NavTab';import BreadCrumb from '../components/BreadCrumb'; 
import Index from '../components/Index';
class Module1 extends Component{state={activities:[{name:'Exercise 1. Initial Stress vs Life Quality Index',link:'/activity/1',id:1},{name:'Exercise 2. Initial Individual Virtue Profile',link:'/activity/2',id:2},{name:'Exercise 3. Other People’s Expectations',
link:'/activity/3',id:3},{name:'Exercise 4. My Expectations from Life',link:'/activity/4',id:4},
{name:'Exercise 5. My Self-Expectations',link:'/activity/5',id:5}]};
render(){return(<div><BreadCrumb active="Module 1"/><NavTab/>
 <Index title="Your Right Reasons" activities={this.state.activities}/></div>)}};export default Module1;
