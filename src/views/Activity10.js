import React, { Component } from 'react';import NavTab from '../components/NavTab';
import BreadCrumb from '../components/BreadCrumb'; import SimpleInfoCard from '../components/SimpleInfoCard'; 
import ActivityTitle from '../components/ActivityTitle'; import TableActivity10 from '../components/TableActivity10';
import TableActivity10a from '../components/TableActivity10a'; import TableActivity10b from '../components/TableActivity10b';
class Activity10 extends Component{state={activities:['Positive professional impacts achieved','Level of problems solved',
  'Profited and recognized professional talents','Tranquility for the money'],
 activities1:['Enjoy the company of others','Enthusiasm-Peace in the current situation of a couple',
  'Appearance of people easy to admire or want','Combining your own efforts with other people'],
 activities2:['Motivation and enthusiasm','Ease to concentrate, solve and create','Rest, sleep and recreation',
  'Absence of addictions, injuries and diseases'],
 activities3:['Overall balance between effort and result','Time performance',
  'Time performanceNew skills, physical, mental, professional','Days different and / or better than the previous'],
 activities4:[{id:'1',virtue:'Protection',meaning:'Identify and eliminate risks for myself and for others.'},
  {id:'2',virtue:'Identity',meaning:'Act authentically knowing that others can observe me.'},
  {id:'3',virtue:'Wisdom',meaning:'Develop my understanding and share it with others.'},
  {id:'4',virtue:'Dignity',meaning:'Give value to myself regardless of my results or the judgments of others.'}, 
  {id:'5',virtue:'Discipline',meaning:'Do what is required as soon as possible.'},
  {id:'6',virtue:'Perseverance',meaning:'Try to change what I know I can change.'},
  {id:'7',virtue:'Admiration',meaning:'Admiring achievements, gifts or attributes of other people.'},
  {id:'8',virtue:'Company',meaning:'Value and thank the company of others trying to be me also good company.'},
  {id:'9',virtue:'Inspiration',meaning:'Let my creative capacity flow, inspiring me in my creations and in those of others.'},
  {id:'10',virtue:'Loyalty',meaning:'Be faithful to the commitments that I have established with myself and with others.'},
  {id:'11',virtue:'Forgiveness',meaning:'Forgiving what others do that affects us by using it for my own growth.'},
  {id:'12',virtue:'Freedom',meaning:'Decide for myself my own behavior based on right reasons.'},
  {id:'13',virtue:'Courage',meaning:'Do what is required even though there are risks'}],
 activities5:[{id:'1',virtue:'Acceptance',
   meaning:'Accept that there are situations that will always be as they are or that it is up to others to change.'},
  {id:'2',virtue:'Justice',meaning:'Let others live the consequences of their behavior.'},
  {id:'3',virtue:'Patience',meaning:'Accept that there is a maximum rate of progress although I want it to be faster.'}],
 activities6:[{id:'1',virtue:'Average of virtues:',result:'1.4'}, 
  {id:'2',virtue:'Your quality of life index - stress is:',result:1.5}]}
render(){return(<div style={{marginBottom:"100px"}}><BreadCrumb active="Exercise 10. Quality of life index - stress"/>
 <NavTab/><SimpleInfoCard cardClass="column is-12" 
  text={["Indicates the value of each of the mentioned aspects. You can enter a new record once a day.", <br/>,  
   <b>1 = very good, 1.5 = good, 2 = fair, 2.5 = bad, 3 = very bad</b>,<br/>,
   "The smaller the number, the better quality of life you have."]}/>
 <ActivityTitle title="Quality of life index - stress" />
 <TableActivity10 header="Éxito:" activities={this.state.activities} footer="Average Success result" id="A"/>
 <TableActivity10 header="Company:" activities={this.state.activities1} footer="Average Company result" id="B"/>
 <TableActivity10 header="Health:" activities={this.state.activities2} footer="Average Health result" id="C"/>
 <TableActivity10 header="Effort:" activities={this.state.activities3} footer="Average Effort result" id="D"/>
 <ActivityTitle title="Attachment to Essential Virtues" />
 <TableActivity10a header="Virtues To Develop" activities={this.state.activities4} footer="Average Effort result"/>
 <TableActivity10a header="Developed Virtues" activities={this.state.activities5} footer="Average Effort result"/>
 <TableActivity10b header="Results" activities={this.state.activities6}/>
 <div className="columns"><div className="column-is-12" style={{marginLeft:"auto", marginRight:"auto"}}>
  <button className="button is-medium is-pulled-right" 
   style={{marginTop:"25px", backgroundColor:"#870404", color:"#FFF"}}>Save Changes</button>
 </div></div></div>)}};export default Activity10;
