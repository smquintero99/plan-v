import React, { Component } from 'react';import NavTab from '../components/NavTab';import BreadCrumb from '../components/BreadCrumb'; import SimpleInfoCard from '../components/SimpleInfoCard'; import ActivityTitle from '../components/ActivityTitle';
import TableActivity12 from '../components/TableActivity12';import TableActivity12a from '../components/TableActivity12a';
class Activity12 extends Component{state={activities:['Protection','Identity','Wisdom','Dignity','Discipline','Perseverance', 'Admiration','Company','Inspiration','Loyalty','Forgiveness','Freedom','Courage'],activities1:['Acceptance','Justice','Patience']}
render(){return(<div><BreadCrumb active="Exercise 12. Purpose of life as a couple"/><NavTab/>
<SimpleInfoCard cardClass="column is-12" text='Answer each question according to the knowledge you have of your partner.'/>
 <ActivityTitle title="Virtues that I learn from my partner"/><TableActivity12 activities={this.state.activities}/>
 <ActivityTitle title="Virtues that my partner learns from me"/><TableActivity12a activities={this.state.activities1}/>
 <ActivityTitle title="Ability to attract and create as a couple"/>
</div>)}}export default Activity12;

