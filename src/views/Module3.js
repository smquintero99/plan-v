import React, { Component } from 'react';import NavTab from '../components/NavTab';import BreadCrumb from '../components/BreadCrumb'; 
import Index from '../components/Index';
class Module3 extends Component{state={activities:[{name:'Exercise 6. My Addictions',link:'/activity/6',id:1},{name:'Exercise 7. The Closed Roads',link:'/activity/7',id:2},{name:'Exercise 9. Ideal Life Project',link:'/activity/9',id:3}]};
render(){return(<div><BreadCrumb active="Module 3"/><NavTab/>
 <Index title="Closed roads and ideal life project" activities={this.state.activities}/></div>)}};export default Module3;
