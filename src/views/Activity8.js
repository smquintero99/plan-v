import React, { Component } from 'react';import NavTab from '../components/NavTab';
import BreadCrumb from '../components/BreadCrumb'; import SimpleInfoCard from '../components/SimpleInfoCard'; 
import TableActivity8 from '../components/TableActivity8'; import {Link} from 'react-router-dom';
class Activity8 extends Component{state={activities:['Dignity','Inspiration','Protection','Identity','Loyalty',
  'Company','Perseverance','Admiration','Forgiveness','Courage','Freedom', 'Discipline','Wisdom'], 
 activities1:['Patience','Acceptance','Justice']}
render(){return(<div style={{marginBottom:"100px"}}><BreadCrumb active="Exercise 8. Your Personal Code of Conduct"/>
 <NavTab/><SimpleInfoCard cardClass="column is-12" text="Write your code of conduct for each Virtue Not Developed and Developed. Write a sentence for each Virtue, starting with: I'm going to ..."/>
 <TableActivity8 header1="Essential Virtues To Develop:" activities={this.state.activities}/>
 <TableActivity8 header1="Developed Essential Virtues:" activities={this.state.activities1}/>
 <div className="columns"><div className="column-is-12" style={{marginLeft:"auto", marginRight:"auto"}}>
  <button className="button is-medium is-pulled-right" style={{marginTop:"25px", backgroundColor:"#870404", color:"#FFF"}}>
   Save Changes</button>
 </div></div> <div className="columns"><div className="column-is-12" style={{marginLeft:"auto", marginRight:"auto"}}>
 <Link to="/module/2"><button className="button is-medium is-pulled-right" 
  style={{marginTop:"25px", backgroundColor:"#000000AA", color:"#FFF"}}>Finalize</button></Link>
 </div></div></div>)}}; export default Activity8;
