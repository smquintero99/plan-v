import React, { Component } from 'react';import ProgressBar from '../components/ProgressBar';
import NavTab from '../components/NavTab';import Carousel from '../components/Carousel';
const Home = ({ classes }) => (<div><ProgressBar /><NavTab /><Carousel /></div>);export default Home;
