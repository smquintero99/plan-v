import React, { Component } from 'react';import NavTab from '../components/NavTab';import BreadCrumb from '../components/BreadCrumb'; 
import Index from '../components/Index';
class Module6 extends Component{state={activities:[{name:'Exercise 12. Purpose of life as a couple',link:'/activity/12',id:1}]};
render(){return(<div><BreadCrumb active="Module 6"/><NavTab/>
 <Index title="Your purpose of life as a couple" activities={this.state.activities}/></div>)}};export default Module6;
